# jsonwrangler

This demonstrates a couple of things:

* How to encode and decode Java POJOs as JSON objects.
* How to UPSERT data.
* How efficient materialized views are 

## How to use

### Create an empty VoltDB on your PC

### Create the schema


* run ddl/db.sql.
*  Note that it assumes that you have the GSON jar files and the contents of 'serverSrc' in a file called jsonwrangler.jar:
```
    load classes ../../jsonwrangler.jar;
    load classes ../gson-2.6.2.jar;
```    
### Start watching

 * Point your web browser at 127.0.0.1:8080 .

### Run the demo

run 'JsonWranglerDemo'
