load classes ../../jsonwrangler.jar;
load classes ../gson-2.6.2.jar;


CREATE table user_table
(userid bigint not null primary key
,user_value_1 varchar(30)
,user_value_2 varchar(30)
,user_value_3 varchar(30)
,user_value_4 varchar(30)
,user_value_5 varchar(30)
,user_value_6 varchar(30)
,user_value_7 varchar(30)
,user_json_object varchar(4096));

create table user_child_table
(userid bigint not null
,thing_name varchar(30) not null
,thing_value varchar(30)
,primary key (userid, thing_name));

echo Partition above tables

PARTITION TABLE user_table ON COLUMN userid;
PARTITION TABLE user_child_table ON COLUMN userid;

echo create views

create view popularity as
select thing_name, thing_value, count(*) how_many
from user_child_table
group by thing_name, thing_value;


CREATE PROCEDURE 
   PARTITION ON TABLE user_table COLUMN userid
   FROM CLASS jsonwrangler.dbproc.UpsertData;
   
CREATE PROCEDURE check_popularity as
select thing_value, how_many 
from popularity
where thing_name = ? 
order by how_many desc, thing_value
limit 20;
