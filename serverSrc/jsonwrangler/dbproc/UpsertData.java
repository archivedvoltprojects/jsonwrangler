package jsonwrangler.dbproc;


/* This file is part of VoltDB.
 * Copyright (C) 2008-2017 VoltDB Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

import org.voltdb.SQLStmt;
import org.voltdb.VoltProcedure;
import org.voltdb.VoltTable;
import org.voltdb.VoltType;

import com.google.gson.Gson;

import jsonwrangler.datastructures.DataPairArray;

public class UpsertData extends VoltProcedure {

    // @formatter:off

    public static final SQLStmt deleteChildren = new SQLStmt("DELETE FROM user_child_table WHERE userid = ?;");
    public static final SQLStmt deleteAChild = new SQLStmt("DELETE FROM user_child_table WHERE userid = ? AND thing_name = ?;");
    public static final SQLStmt upsertAChild = new SQLStmt("UPSERT INTO user_child_table (userid, thing_name, thing_value) VALUES (?,?,?);");
    public static final SQLStmt upsertUserTable = new SQLStmt("UPSERT INTO user_table (userid,  user_json_object) VALUES (?,?);");
    public static final SQLStmt upsertUserTable2 = new SQLStmt("UPSERT INTO user_table (userid, user_value_1 ) VALUES (?,?);");

    // @formatter:on

    StringBuffer statusString = null;
    
    Gson g = null;

    public VoltTable[] run(long userId, String clobber, String payload) throws VoltAbortException {

        if (g == null) {
            g = new Gson();
        }
        
        if (clobber.equalsIgnoreCase("Y")) {
            voltQueueSQL(deleteChildren,userId);
        }
        
        voltQueueSQL(upsertUserTable,userId, payload );
        
        DataPairArray dpa = g.fromJson(payload, DataPairArray.class);
        
        for (int i=0; i < dpa.pairValues.length; i++) {
            if (  dpa.pairValues[i].name.equals("SCHOOL_0")) {
                voltQueueSQL(upsertUserTable2,userId, dpa.pairValues[i].value);
            } else {
                if (dpa.pairValues[i].delete) {
                    // Delete
                    voltQueueSQL(deleteAChild, userId, dpa.pairValues[i].name );
                } else {
                    // Upsert
                    voltQueueSQL(upsertAChild, userId, dpa.pairValues[i].name, dpa.pairValues[i].value );
                }
            }
        }

        return voltExecuteSQL(true);
    }

   

}
