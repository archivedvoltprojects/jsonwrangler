package jsonwrangler.clientcode;

/* This file is part of VoltDB.
 * Copyright (C) 2008-2018 VoltDB Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */


import java.util.Random;

import jsonwrangler.datastructures.DataPair;
import jsonwrangler.datastructures.DataPairArray;

public class DataPairGenerator {

    public static final String[] STATIONS = { "ADDISCOMBE TRAM", "ADDNGTN VIL TRAM", "AMPERE WAY TRAM", "ARENA TRAM",
            "AVENUE ROAD TRAM", "Acton Central", "Acton Main Line", "Acton Town", "Aldgate", "Aldgate East",
            "All Saints", "Alperton", "Amersham", "Angel", "Archway", "Arnos Grove", "Arsenal", "BECKENHM JN TRAM",
            "BECKENHM RD TRAM", "BEDDNGTN LN TRAM", "BELGRAVE WK TRAM", "BIRKBECK TRAM", "Baker Street", "Balham",
            "Balham NR", "Balham SCL", "Bank", "Barbican", "Barking", "Barkingside", "Barons Court", "Battersea Park",
            "Bayswater", "Beckton", "Beckton Park", "Becontree", "Bellingham", "Belsize Park", "Bermondsey",
            "Bethnal Green", "Bethnal Green NR", "Blackfriars", "BLCKHRS LNE TRAM", "Blackhorse Road", "Blackwall",
            "Bond Street", "Borough", "Boston Manor", "Bounds Green", "Bow Church", "Bow Road", "Brent Cross",
            "Brixton", "Brockley", "Bromley By Bow", "Brondesbury", "Brondesbury Park", "Buckhurst Hill", "Burnt Oak",
            "Bus", "Bushey", "CHURCH STRT TRAM", "COOMBE LANE TRAM", "Caledonian Rd&B'sby", "Caledonian Road",
            "Cambridge Heath", "Camden Road", "Camden Town", "Canada Water", "Canary Wharf", "Canary Wharf DLR",
            "Canary Wharf E2", "Canning Town", "Cannon Street", "Canonbury", "Canons Park", "Carpenders Park",
            "Carshalton", "Castle Bar Park", "Chalfont & Latimer", "Chalk Farm", "Chancery Lane", "Charing Cross",
            "Chesham", "Chigwell", "Chiswick Park", "Chorleywood", "City Thameslink", "Clapham Common",
            "Clapham Junction", "Clapham North", "Clapham South", "Clapton", "Cockfosters", "Colindale",
            "Colliers Wood", "Covent Garden", "Crossharbour", "Crouch Hill", "Croxley", "Crystal Palace",
            "Custom House DLR", "Cutty Sark", "Cyprus", "DUNDONLD RD TRAM", "Dagenham Dock", "Dagenham East",
            "Dagenham Heathway", "Dalston Kingsland", "Debden", "Deptford Bridge", "Devons Road", "Dollis Hill",
            "Drayton Green", "Drayton Pk", "EAST CROYDON TRAM", "ELMERS END TRAM", "Ealing Broadway", "Ealing Common",
            "Earls Court", "East Acton", "East Croydon", "East Finchley", "East Ham", "East India", "East Putney",
            "Eastcote", "Edgware", "Edgware Road B", "Edgware Road M", "Elephant & Castle", "Elm Park", "Elverson Road",
            "Embankment", "Epping", "Essex Road", "Euston", "Euston NR", "Euston Square", "FENCHURCH ST NR",
            "FIELD WAY TRAM", "Fairlop", "Farringdon", "Fenchurch St NR", "Finchley Central", "Finchley Rd & Frognal",
            "Finchley Road", "Finsbury Park", "Forest Hill", "Fulham Broadway", "GEORGE STRT TRAM", "GRAVEL HILL TRAM",
            "Gallions Reach", "Gants Hill", "Gloucester Road", "Golders Green", "Goldhawk Road", "Goodge Street",
            "Gospel Oak", "Grange Hill", "Great Portland St", "Green Park", "Greenford", "Greenwich DLR", "Gunnersbury",
            "HARRNGTN RD TRAM", "Hackney Central", "Hackney Downs", "Hackney Wick", "Hainault", "Hammersmith D",
            "Hammersmith M", "Hampstead", "Hampstead Heath", "Hanger Lane", "Hanwell", "Harlesden",
            "Harringay Green Las", "Harrow On The Hill", "Harrow Wealdstone", "Hatch End", "Hatton Cross",
            "Hayes & Harlington", "Headstone Lane", "Heathrow Term 4", "Heathrow Term 5", "Heathrow Terms 123",
            "Harringay", "Hendon Central", "Heron Quays", "High Barnet", "High Street Kens", "Highbury", "Highgate",
            "Hillingdon", "Holborn", "Holland Park", "Holloway Road", "Homerton", "Honor Oak Park", "Hornchurch",
            "Hounslow Central", "Hounslow East", "Hounslow West", "Hyde Park Corner", "Ickenham", "Ilford",
            "Imperial Wharf", "Island Gardens", "KING HENRYS TRAM", "Kennington", "Kensal Green", "Kensal Rise",
            "Kensington Olympia", "Kentish Town", "Kentish Town West", "Kenton", "Kew Gardens", "Kilburn",
            "Kilburn High Road", "Kilburn Park", "King George V", "Kings Cross", "Kings Cross M", "Kings Cross T",
            "Kingsbury", "Knightsbridge", "LEBANON RD TRAM", "LLOYD PARK TRAM", "Ladbroke Grove", "Lambeth North",
            "Lancaster Gate", "Langdon Park", "Latimer Road", "Leicester Square", "Lewisham DLR", "Leyton",
            "Leyton Midland Road", "Leytonstone", "Leytonstone High Rd", "Limehouse DLR", "Limehouse NR",
            "Liverpool St NR", "Liverpool St WAGN TOC Gates", "Liverpool Street", "London Bridge",
            "London City Airport", "London Fields", "Loughton", "MERTON PARK TRAM", "MITCHAM JCN TRAM", "MITCHAM TRAM",
            "MORDEN ROAD TRAM", "Maida Vale", "Manor House", "Mansion House", "Marble Arch", "Marylebone",
            "Marylebone NR", "Mile End", "Mill Hill East", "Monument", "Moor Park", "Moorgate", "Morden",
            "Mornington Crescent", "Mudchute", "NEW ADDNGTH TRAM", "Neasden", "New Cross", "Newbury Park", "Norbury",
            "North Acton", "North Ealing", "North Greenwich", "North Harrow", "North Wembley", "Northfields",
            "Northolt", "Northolt Park", "Northwick Park", "Northwood", "Northwood Hills", "Norwood Junction SR",
            "Notting Hill Gate", "Oakwood", "Old Street", "Osterley", "Oval", "Oxford Circus", "PHIPPS BRDG TRAM",
            "Paddington", "Paddington FGW", "Park Royal", "Parsons Green", "Peckham Rye", "Perivale",
            "Piccadilly Circus", "Pimlico", "Pinner", "Plaistow", "Pontoon Dock", "Poplar", "Preston Road",
            "Prince Regent", "Pudding Mill Lane", "Purley", "Putney Bridge", "Queens Park", "Queensbury", "Queensway",
            "REEVES CRNR TRAM", "Rainham Essex", "Ravenscourt Park", "Rayners Lane", "Rectory Road", "Redbridge",
            "Regents Park", "Richmond", "Rickmansworth", "Roding Valley", "Romford", "Royal Albert", "Royal Oak",
            "Royal Victoria", "Ruislip", "Ruislip Gardens", "Ruislip Manor", "Russell Square", "SANDILANDS TRAM",
            "Seven Sisters", "Shadwell DLR", "Shepherd's Bush Mkt", "Shepherd's Bush NR", "Shepherd's Bush Und",
            "Sloane Square", "Snaresbrook", "South Acton", "South Croydon", "South Ealing", "South Greenford",
            "South Hampstead", "South Harrow", "South Kensington", "South Kenton", "South Quay", "South Ruislip",
            "South Tottenham", "South Wimbledon", "South Woodford", "Southall", "Southfields", "Southgate", "Southwark",
            "St James Street", "St James's Park", "St Johns Wood", "St Pancras International", "St Pauls",
            "Stamford Brook", "Stamford Hill", "Stanmore", "Stepney Green", "Stockwell", "Stoke Newington",
            "Stonebridge Park", "Stratford", "Streatham", "Streatham Common", "Sudbury Hill", "Sudbury Hill Harrow",
            "Sudbury Town", "Sudbury&Harrow Rd", "Sutton Surrey", "Swiss Cottage", "Sydenham SR", "TAMWORTH RD TRAM",
            "THERAPIA LN TRAM", "Temple", "Theydon Bois", "Thornton Heath", "Tooting Bec", "Tooting Broadway",
            "Tottenham Court Rd", "Tottenham Hale", "Totteridge", "Tower Gateway", "Tower Hill", "Tufnell Park",
            "Tulse Hill", "Turnham Green", "Turnpike Lane", "Upminster", "Upminster Bridge", "Upney", "Upper Holloway",
            "Upton Park", "Uxbridge", "Vauxhall", "Victoria", "Victoria TOCs", "WADDON MARSH TRAM", "WANDLE PARK TRAM",
            "WELLESLY RD TRAM", "WEST CROYDON TRAM", "WIMBLEDON TRAM", "WOODSIDE TRAM", "Wallington",
            "Walthamstow Central", "Walthamstow Qns R", "Wanstead", "Wanstead Park", "Warren Street", "Warwick Avenue",
            "Waterloo", "Waterloo JLE", "Watford High Street", "Watford Junction", "Watford Met", "Wembley Central",
            "Wembley Park", "Wembley Stadium", "West Acton", "West Brompton", "West Croydon", "West Drayton",
            "West Ealing", "West Finchley", "West Ham", "West Hampst'd NL", "West Hampst'd Tlink", "West Hampstead",
            "West Harrow", "West India Quay", "West Kensington", "West Norwood", "West Ruislip", "West Silvertown",
            "Westbourne Park", "Westferry", "Westminster", "White City", "Whitechapel", "Willesden Green",
            "Willesden Junction", "Wimbledon", "Wimbledon Park", "Wood Green", "Wood Lane", "Woodford",
            "Woodgrange Park", "Woodside Park", "Woolwich Arsenal DLR" };

    public final static String[] PLACES_OF_INTEREST = { "MY HOUSE", "SCHOOL", "WORK", "DENTIST", "ACCOUNTANT",
            "TAXIDERMIST" };

    public static DataPairArray getNewArray(long userId, boolean clobber, int elementCount, Random r) {

        DataPair[] newValues = new DataPair[elementCount];

        for (int i = 0; i < newValues.length; i++) {

            String attributeName = PLACES_OF_INTEREST[i % PLACES_OF_INTEREST.length] + i % PLACES_OF_INTEREST.length;

            double latestPick = r.nextGaussian() ;
            
            while (latestPick < 0 || latestPick > 1) {
                latestPick = r.nextGaussian() ;
            }
            
            double randomPick = latestPick ;
            
            
            
            if (randomPick < 0) {
                randomPick *= -1;
            }
            
            
            randomPick *= STATIONS.length;
  
            String attributeValue = STATIONS[(int)randomPick];
            boolean deleteThisElement = false;

            newValues[i] = new DataPair(attributeName, attributeValue, deleteThisElement);
        }

        DataPairArray newArray = new DataPairArray(userId, clobber, newValues);

        return newArray;

    }

}
