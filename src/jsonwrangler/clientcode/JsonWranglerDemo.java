package jsonwrangler.clientcode;

/* This file is part of VoltDB.
 * Copyright (C) 2008-2018 VoltDB Inc.
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 */

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Random;

import org.voltdb.client.Client;
import org.voltdb.client.ClientConfig;
import org.voltdb.client.ClientFactory;
import org.voltdb.client.ClientResponse;

import com.google.gson.Gson;

import jsonwrangler.datastructures.DataPairArray;

public class JsonWranglerDemo {

    public static void main(String[] args) {

        Random r = new Random();
        Gson g = new Gson();

        int recordCount = 100000;

        // Create an array of objects, and their GSON representations...
        DataPairArray[] data = new DataPairArray[recordCount];
        String[] dataAsString = new String[recordCount];

        msg("Creating " + recordCount + " Java objects...");

        for (int i = 0; i < data.length; i++) {
            data[i] = DataPairGenerator.getNewArray(i, false, 10 + (i % 20), r);
        }

        msg("Turning " + recordCount + " Java objects into JSON objects...");

        final long startMs = System.currentTimeMillis();

        for (int i = 0; i < data.length; i++) {
            dataAsString[i] = g.toJson(data[i]);
        }

        long entriesPerMs = recordCount / (System.currentTimeMillis() - startMs);

        msg("GSON'd " + entriesPerMs + " entries per ms...");
        
        // For giggles see how fast it is to turn JSON into objects.
        // VoltDB will have to do this when we call UpsertData 

        msg("Recreate Java objects from JSON...");
        final long startMsRecreate = System.currentTimeMillis();
        for (int i = 0; i < data.length; i++) {
            DataPairArray foo = g.fromJson(dataAsString[i], DataPairArray.class);
        }

        entriesPerMs = recordCount / (System.currentTimeMillis() - startMsRecreate);
        msg("un-GSON'd " + entriesPerMs + " entries per ms...");
        
        // Now iterate through our array and call UpsertData...

        try {
            Client c = connectVoltDB("localhost");
            ComplainOnErrorCallback coec = new ComplainOnErrorCallback();

            final long startMsUpsert = System.currentTimeMillis();

            for (int i = 0; i < data.length; i++) {

                c.callProcedure(coec, "UpsertData", i, "N", dataAsString[i]);

                if (i % 10000 == 0) {
                    
                    // Every now and then we hit the materialized view. Note how 
                    // this *doesn't* impact performance.
                    msg("On row " + i);

                    PrintResultsCallback prc = new PrintResultsCallback();
                    c.callProcedure(prc, "check_popularity", "WORK2");
                  
                }

 
            }

            // Remember: We're async! Stuff is stuff happening when we get to the next 
            // lines of code...
            msg("All entries in queue, waiting for it to drain...");
            c.drain();

            msg("Queue drained...");

            entriesPerMs = recordCount / (System.currentTimeMillis() - startMsUpsert);
            msg("upserted " + entriesPerMs + " entries per ms...");

            msg("Closing connection...");

            c.close();

        } catch (Exception e) {
            msg(e.getMessage());
        }

    }

    public static void msg(String message) {

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date now = new Date();
        String strDate = sdfDate.format(now);
        System.out.println(strDate + ":" + message);
    }

    private static Client connectVoltDB(String hostname) throws Exception {
        Client client = null;
        ClientConfig config = null;

        try {
            msg("Logging into VoltDB");

            config = new ClientConfig(); // "admin", "idontknow");
            config.setMaxOutstandingTxns(20000);
            config.setMaxTransactionsPerSecond(200000);
            config.setTopologyChangeAware(true);
            config.setReconnectOnConnectionLoss(true);

            client = ClientFactory.createClient(config);

            String[] hostnameArray = hostname.split(",");

            for (int i = 0; i < hostnameArray.length; i++) {
                msg("Connect to " + hostnameArray[i] + "...");
                try {
                    client.createConnection(hostnameArray[i]);
                } catch (Exception e) {
                    msg(e.getMessage());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception("VoltDB connection failed.." + e.getMessage(), e);
        }

        return client;

    }

}
